import {environment} from './environments/environment';
import {enableProdMode} from '@angular/core';
import 'localstorage-polyfill';

export { AppServerModule } from './app/app.server.module';

export { renderModule, renderModuleFactory } from '@angular/platform-server';

global['localStorage'] = localStorage;

if (environment.production) {
  enableProdMode();
}
