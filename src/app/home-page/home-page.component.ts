import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {TransferHttpService} from '@gorniv/ngx-universal';
import {MetaService} from '@ngx-meta/core';
import {UniversalStorage} from '@shared/storage/universal.storage';
import {DOCUMENT} from '@angular/common';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {Meta} from '@angular/platform-browser';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [
    trigger('shadow', [
      state('begin', style({
        boxShadow: '-1px 7px 23px -6px rgba(0, 0, 0, 0.34)'
      })),
      state('end', style({
        boxShadow: '-1px 7px 23px -6px rgba(0, 0, 0, 0.14)',
      })),
      transition('begin => end', animate('0.3s')),
      transition('* => *', animate('0.3s'))
    ])
  ]
})
export class HomePageComponent implements OnInit {
  errorMessage: string;
  portfolio_block_opened: boolean = false;
  action = 'begin';
  site_types = [
    {
      image: '/assets/images/landing.jpg',
      title: 'data.site_types.0.title',
    },
    {
      image: '/assets/images/site.jpg',
      title: 'data.site_types.1.title',
    },
    {
      image: '/assets/images/corp-site.jpg',
      title: 'data.site_types.2.title',
    },
    {
      image: '/assets/images/shop.jpg',
      title: 'data.site_types.3.title',
    },
    {
      image: '/assets/images/app.jpg',
      title: 'data.site_types.4.title',
    }
  ];
  cards = [
    {
      image: '/assets/images/modern-cms.jpg',
      title: 'data.cards.0.title',
      text: 'data.cards.0.text'
    },
    {
      image: '/assets/images/mobile-version.jpg',
      title: 'data.cards.1.title',
      text: 'data.cards.1.text'
    },
    {
      image: '/assets/images/high-speed.jpg',
      title: 'data.cards.2.title',
      text: 'data.cards.2.text'
    },
    {
      image: '/assets/images/safety.jpg',
      title: 'data.cards.3.title',
      text: 'data.cards.3.text'
    }
  ];
  technologies = [
    {
      icon: '/assets/icons/technologies/vue-logo.png',
      name: 'Vue js'
    },
    {
      icon: '/assets/icons/technologies/angular-152x152.png',
      name: 'Angular'
    },
    {
      icon: '/assets/icons/technologies/react-logo.png',
      name: 'React'
    },
    {
      icon: '/assets/icons/technologies/laravel.png',
      name: 'Laravel'
    },
    {
      icon: '/assets/icons/technologies/php-logo.png',
      name: 'Php'
    },
    {
      icon: '/assets/icons/technologies/js-logo.png',
      name: 'Java Script'
    },
    {
      icon: '/assets/icons/technologies/css-logo.png',
      name: 'Css 3'
    },
    {
      icon: '/assets/icons/technologies/html-logo.png',
      name: 'Html 5'
    },
    {
      icon: '/assets/icons/technologies/wp-logo.png',
      name: 'Wordpress'
    },
    {
      icon: '/assets/icons/technologies/dle-logo.png',
      name: 'DLE'
    }
  ];

  public translateText;
  public translateDesc;
  constructor(
    @Inject(PLATFORM_ID) private _platformId: Object,
    private _http: TransferHttpService,
    private readonly _meta: MetaService,
    private meta: Meta,
    private _universalStorage: UniversalStorage,
    translate: TranslateService,
    // instead window.document
    @Inject(DOCUMENT) private _document: Document,
  ) {
    translate.onLangChange.pipe().subscribe((event: LangChangeEvent) => {
      translate.get('home.seo_title').subscribe(text => this.translateText = text);
      translate.get('home.seo_desc').subscribe(text => this.translateDesc = text);
      this._meta.setTitle(this.translateText);
      this._meta.setTag('description', this.translateDesc);
    });
    translate.get('home.seo_title').subscribe(text => this.translateText = text);
    translate.get('home.seo_desc').subscribe(text => this.translateDesc = text);
  }
  ngOnInit(): void {
    this._meta.setTitle(this.translateText);
    this._meta.setTag('twitter:title', this.translateText);
    this._meta.setTag('description', this.translateDesc);
    this._meta.setTag('twitter:description', this.translateDesc);
    this._meta.setTag('author', 'BESTSITE');
    this._meta.setTag('site_name', 'BESTSITE');
    this._meta.setTag('twitter:card', this.translateText);
    this._meta.setTag('keywords', 'Develop Sites, Applications, Technologies');

    this.meta.addTag({ name: 'image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'og:image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'twitter:image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image'});
    this._meta.setTag('og:type', 'website');
    this.meta.addTag({ name: 'url', content: '//bestsite.az'});
    this.meta.addTag({ name: 'og:url', content: '//bestsite.az'});
    this.meta.addTag({ name: 'twitter:url', content: '//bestsite.az'});
  }
}
