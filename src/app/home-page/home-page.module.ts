import { NgModule } from '@angular/core';
import {TechnologiesComponent} from "@shared/components/technologies/technologies.component";
import {PortfolioSingleComponent} from "@shared/components/portfolio-single/portfolio-single.component";
import {SwiperCompaniesComponent} from "@shared/components/swiper-companies/swiper-companies.component";
import {TopSliderComponent} from "@shared/components/top-slider/top-slider.component";
import {ContactUsComponent} from "@shared/components/contact-us/contact-us.component";
import {CardComponent} from "@shared/components/card/card.component";
import {SharedModule} from "@shared/shared.module";
import {SwiperModule} from 'ngx-swiper-wrapper';

@NgModule({
  imports: [
    SharedModule,
    SwiperModule
  ],
  declarations: [
    TopSliderComponent,
    PortfolioSingleComponent,
    TechnologiesComponent,
    SwiperCompaniesComponent,
    ContactUsComponent,
    CardComponent
  ],
  exports:  [
    TopSliderComponent,
    PortfolioSingleComponent,
    TechnologiesComponent,
    SwiperCompaniesComponent,
    ContactUsComponent,
    CardComponent
  ]
})
export class HomePageModule {

}
