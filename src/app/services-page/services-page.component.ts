import { Component, OnInit } from '@angular/core';
import {MetaService} from '@ngx-meta/core';
import {Meta} from '@angular/platform-browser';

@Component({
  selector: 'app-services-page',
  templateUrl: './services-page.component.html',
  styleUrls: ['./services-page.component.css']
})
export class ServicesPageComponent implements OnInit {

  public services = [];
  constructor(private readonly _meta: MetaService, private meta: Meta) { }

  ngOnInit() {
    for (let i = 0; i < 4; i++) {
      this.services.push({
        'title': 'services.' + i + '.title',
        'text': 'services.' + i + '.text',
        'image': '/assets/images/services/' + i + '.jpg'
      });
    }

    this._meta.setTitle('services.seo.title');
    this._meta.setTag('twitter:title', 'services.seo.title');
    this._meta.setTag('description', 'services.seo.desc');
    this._meta.setTag('twitter:description', 'services.seo.desc');
    this._meta.setTag('author', 'BESTSITE');
    this._meta.setTag('site_name', 'BESTSITE');
    this._meta.setTag('twitter:card', 'services.seo.title');
    this._meta.setTag('keywords', 'Develop Sites, Applications, Technologies');

    this.meta.addTag({ name: 'image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'twitter:image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image'});
    this._meta.setTag('og:type', 'website');
    this.meta.addTag({ name: 'url', content: '//bestsite.az/our-services'});
    this.meta.addTag({ name: 'og:url', content: '//bestsite.az/our-services'});
    this.meta.addTag({ name: 'twitter:url', content: '//bestsite.az/our-services'});
  }

  isInteger(num) {
    return num % 2;
  }
}
