import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MainLayoutComponent} from '@shared/components/main-layout/main-layout.component';
import {SharedModule} from '@shared/shared.module';
import {ServicesPageComponent} from './services-page.component';

@NgModule({
  declarations: [
    ServicesPageComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', pathMatch: 'full', component: MainLayoutComponent, children: [
          {path: '', component: ServicesPageComponent}
        ]
      }
    ]),
    SharedModule
  ],
  exports: [
    ServicesPageComponent
  ],
  bootstrap: [
    ServicesPageComponent
  ]
})

export class ServicesPageModule {

}
