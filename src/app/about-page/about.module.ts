import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AboutPageComponent} from './about-page.component';
import {MainLayoutComponent} from '@shared/components/main-layout/main-layout.component';
import {SharedModule} from '@shared/shared.module';

@NgModule({
  declarations: [
    AboutPageComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', pathMatch: 'full', component: MainLayoutComponent, children: [
          {path: '', component: AboutPageComponent}
        ]
      }
    ]),
    SharedModule
  ],
  exports: [
    AboutPageComponent
  ],
  bootstrap: [
    AboutPageComponent
  ]
})
export class AboutModule {
  static forRoot(): ModuleWithProviders<AboutModule> {
    return { ngModule: AboutModule };
  }
}
