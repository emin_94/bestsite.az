import { Component, OnInit } from '@angular/core';
import {MetaService} from '@ngx-meta/core';
import {Meta} from '@angular/platform-browser';

@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.css']
})
export class AboutPageComponent implements OnInit {

  constructor(private readonly _meta: MetaService, private meta: Meta) { }

  ngOnInit(): void {
    this._meta.setTitle('about.seo.title');
    this._meta.setTag('twitter:title', 'about.seo.title');
    this._meta.setTag('description', 'about.seo.desc');
    this._meta.setTag('twitter:description', 'about.seo.desc');
    this._meta.setTag('author', 'BESTSITE');
    this._meta.setTag('site_name', 'BESTSITE');
    this._meta.setTag('twitter:card', 'about.seo.title');
    this._meta.setTag('keywords', 'Develop Sites, Applications, Technologies');

    this.meta.addTag({ name: 'image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'og:image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'twitter:image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image'});
    this._meta.setTag('og:type', 'website');
    this._meta.setTag('og:url', '//bestsite.az/about-us');
    this._meta.setTag('twitter:url', '//bestsite.az/about-us');
  }

}
