import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import { MetaGuard } from '@ngx-meta/core';

import {MainLayoutComponent} from '@shared/components/main-layout/main-layout.component';
import {HomePageComponent} from './home-page/home-page.component';
import {NotFoundComponent} from './not-found/not-found.component';

const routes: Routes = [
  {
    path: '', component: MainLayoutComponent, children: [
      {path: '', redirectTo: '/', pathMatch: 'full', canActivateChild: [MetaGuard]},
      {path: '', component: HomePageComponent},
    ]
  },
  {path: 'about-us', loadChildren: () => import('./about-page/about.module').then(m => m.AboutModule)},
  {path: 'our-services', loadChildren: () => import('./services-page/services-page.module').then(m => m.ServicesPageModule)},
  {path: 'blog', loadChildren: () => import('./blog-page/blog-page.module').then(m => m.BlogPageModule)},
  {path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)},
  { // 404
    path: '', component: MainLayoutComponent, children: [
      {path: '**', component: NotFoundComponent}
    ]
  }

  // {
  //   path: 'admin',
  //   pathMatch: 'full',
  //   component: WrapperComponent,
  //   canActivateChild: [MetaGuard],
  //   children: [{ path: '', loadChildren: () => import('./admin/shared/components/admin-layout/admin-layout.component') }],
  // },
  // {
  //   path: '',
  //   component: WrapperComponent,
  //   canActivateChild: [MetaGuard],
  //   children: [{ path: '**', loadChildren: () => import('./not-found/not-found.module').then(m => m.NotFoundModule) }],
  // },
];
// must use {initialNavigation: 'enabled'}) - for one load page, without reload
export const AppRoutes = RouterModule.forRoot(routes);
