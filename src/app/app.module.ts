// angular
import {APP_ID, APP_INITIALIZER, Inject, NgModule, PLATFORM_ID, Provider} from '@angular/core';
import { RouterModule } from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// libs
import { CookieService, CookieModule } from '@gorniv/ngx-universal';
import { TransferHttpCacheModule } from '@nguniversal/common';
// shared
import { SharedModule } from '@shared/shared.module';
import { TranslatesService } from '@shared/translates';
// components
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { UniversalStorage } from '@shared/storage/universal.storage';
import { MainLayoutComponent } from '@shared/components/main-layout/main-layout.component';
import { HomePageComponent } from './home-page/home-page.component';
import { CardComponent } from '@shared/components/card/card.component';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from './material-module';
import { FooterComponent } from '@shared/components/footer/footer.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SharedMetaModule } from '@shared/shared-meta';
import {AuthInterceptor} from './auth.interceptor';

export function initLanguage(translateService: TranslatesService): Function {
  return (): Promise<any> => translateService.initLanguage();
}

const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AuthInterceptor
};

@NgModule({
  imports: [
    AppRoutes,
    RouterModule,
    SharedMetaModule,
    SharedModule.forRoot(),
    MaterialModule,
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    TransferHttpCacheModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CookieModule.forRoot(),
    BrowserAnimationsModule,
    TranslateModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    MainLayoutComponent,
    HomePageComponent,
    CardComponent,
    FooterComponent,
    NotFoundComponent,
  ],
  providers: [
    INTERCEPTOR_PROVIDER,
    CookieService,
    UniversalStorage,
    { provide: APP_INITIALIZER, useFactory: initLanguage, multi: true, deps: [TranslatesService] },
  ],
})
export class AppModule {

}
