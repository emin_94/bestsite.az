import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MainLayoutComponent} from '@shared/components/main-layout/main-layout.component';
import {SharedModule} from '@shared/shared.module';
import {BlogPageComponent} from './blog-page.component';
import {PostPageComponent} from '../post-page/post-page.component';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    BlogPageComponent,
    PostPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: MainLayoutComponent, children: [
          {path: '', pathMatch: 'full', component: BlogPageComponent},
          {path: '12312', component: PostPageComponent}
        ]
      },
    ]),
    SharedModule,
    MatButtonModule
  ],
  exports: [
    RouterModule,
    BlogPageComponent,
    PostPageComponent
  ],
  bootstrap: [
    BlogPageComponent
  ]
})
export class BlogPageModule {
  static forRoot(): ModuleWithProviders<BlogPageModule> {
    return { ngModule: BlogPageModule };
  }
}
