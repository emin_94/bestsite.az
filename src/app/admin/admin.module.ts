import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './shared/components/admin-layout/admin-layout.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { MaterialModule } from '../material-module';
import { NotFoundComponent } from '../not-found/not-found.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedAdminModule } from '@shared/shared.admin.module';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { SharedModule } from '@shared/shared.module';
import { AuthGuard } from './shared/services/auth.guard';
import { CreatePageComponent } from './create-page/create-page.component';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { SearchPipe } from './shared/pipes/search.pipe';
import { EditPageComponent } from './edit-page/edit-page.component';

@NgModule({
  declarations: [AdminLayoutComponent, LoginPageComponent, DashboardPageComponent, CreatePageComponent, SearchPipe, EditPageComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedAdminModule,
    RouterModule.forChild([
      {
        path: '', component: AdminLayoutComponent, children: [
          {path: '', redirectTo: '/admin/dashboard', pathMatch: 'full'},
          {path: 'login', component: LoginPageComponent},
          {path: 'dashboard', component: DashboardPageComponent, canActivate: [AuthGuard]},
          {path: 'create', component: CreatePageComponent, canActivate: [AuthGuard]},
          {path: ':id/edit', component: EditPageComponent, canActivate: [AuthGuard]},
          {path: '**', component: NotFoundComponent, canActivate: [AuthGuard]}
        ]
      }
    ]),
    SharedModule,
    MatSelectModule,
    MatButtonToggleModule,
  ],
  exports: [RouterModule, SearchPipe],
  providers: [AuthGuard]
})
export class AdminModule { }
