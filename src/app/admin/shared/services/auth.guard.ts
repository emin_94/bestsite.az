import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router, ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {AuthService} from "@shared/services/auth.service";
import {isPlatformBrowser, isPlatformServer} from "@angular/common";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
    private auth: AuthService,
    private router: Router,
  ){}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.auth.isAuthenticated()) {
      return true
    }else {
      this.auth.logout()
      this.router.navigate(['/admin', 'login'], {
        queryParams: {
          loginAgain: true
        }
      })
    }
  }

}
