import { Component, OnInit } from '@angular/core';
import {Post} from '@shared/interfaces';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PostService} from '@shared/services/post.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss', '../admin-styles.scss']
})
export class CreatePageComponent implements OnInit {
  form: FormGroup;

  constructor(private postServoce: PostService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      text: new FormControl(null, [Validators.required, Validators.minLength(10)]),
      category: new FormControl(null, [Validators.required]),
    });
  }

  submit() {
    if (this.form.invalid) {
      return 'Не удалось сохранить.';
    }

    const post: Post = {
      title: this.form.value.title,
      text: this.form.value.text,
      category: this.form.value.category,
      date: new Date()
    };

    this.postServoce.create(post).subscribe(() => {
      this.form.reset();
    });
  }
}
