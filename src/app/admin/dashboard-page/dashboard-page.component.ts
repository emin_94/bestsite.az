import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '@shared/services/auth.service';
import {PostService} from '@shared/services/post.service';
import {Post} from '@shared/interfaces';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})

export class DashboardPageComponent implements OnInit, OnDestroy {

  posts: Post[] = [];
  pSub: Subscription;
  dSub: Subscription;

  searchStr = '';
  loading: boolean = false;

  displayedColumns: string[] = ['position', 'title', 'date', 'actions'];

  constructor(
    private auth: AuthService,
    private postService: PostService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loading = true;
    this.pSub = this.postService.getAll().subscribe(posts => {
      this.posts = posts;
      this.loading = false;
    });
  }
  ngOnDestroy() {
    if (this.pSub) {
      this.pSub.unsubscribe();
    }
    if (this.dSub) {
      this.dSub.unsubscribe();
    }
  }

  edit(id: string) {
    this.router.navigate(['/admin', id, 'edit']);
  }
  remove(id: string) {
    this.dSub = this.postService.remove(id).subscribe(() => {
      this.posts = this.posts.filter(post => post.id !== id);
    });
  }
}
