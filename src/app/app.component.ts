import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';
import * as AOS from 'aos';
import {SwUpdate} from '@angular/service-worker';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {
  constructor(@Inject(PLATFORM_ID) private platformId: any ) {
  }
  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      AOS.init({
        duration: 1500
      });
      setTimeout(() => {
        AOS.refresh();
      }, 500);
    }
  }
}
