import { ModuleWithProviders, NgModule } from '@angular/core';
import { TransferHttpModule } from '@gorniv/ngx-universal';
import { LayoutsModule } from './layouts/layouts.module';
import { TechnologiesComponent } from './components/technologies/technologies.component';
import { PortfolioSingleComponent } from '@shared/components/portfolio-single/portfolio-single.component';
import { SwiperCompaniesComponent } from './components/swiper-companies/swiper-companies.component';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { TopSliderComponent } from './components/top-slider/top-slider.component';
import { TranslateModule } from '@ngx-translate/core';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material-module';
import { ScrollToIdDirective } from './directives/scroll-to-id.directive';
import { SmallHeaderComponent } from './components/small-header/small-header.component';
import { PostComponent } from '@shared/components/post/post.component';
import { RouterModule } from '@angular/router';
import {SharedAdminModule} from '@shared/shared.admin.module';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};
@NgModule({
  exports: [LayoutsModule, TransferHttpModule, TranslateModule, TechnologiesComponent, PortfolioSingleComponent, SwiperCompaniesComponent, TopSliderComponent, ContactUsComponent, ScrollToIdDirective, SmallHeaderComponent, PostComponent],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ],
  declarations: [TechnologiesComponent, PortfolioSingleComponent, SwiperCompaniesComponent, TopSliderComponent, ContactUsComponent, ScrollToIdDirective, SmallHeaderComponent, PostComponent],
  imports: [
    MaterialModule,
    TranslateModule,
    ReactiveFormsModule,
    SwiperModule,
    RouterModule,
    SharedAdminModule
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return { ngModule: SharedModule };
  }
}
