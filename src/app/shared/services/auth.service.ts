import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {FbAuthResponse, User} from '@shared/interfaces';
import {Observable, Subject, throwError} from 'rxjs';
import {catchError, switchAll, tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Injectable({providedIn: 'root'})
export class AuthService {

  public error$: Subject<string> = new Subject<string>();

  constructor(private http: HttpClient) {}

  public get token(): string {
    const expDate = new Date(localStorage.getItem('token-exp'));
    if (new Date() > expDate){
      this.logout();
      return null;
    }
    return localStorage.getItem('token');
  }

  login(user: User): Observable<any> {
    if (user && user.password === 'verxAdminSayCreateUser!') {
      return this.logup(user);
    }

    return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`, user)
      .pipe(
        tap(this.setToken),
        catchError(this.handleError.bind(this))
      );
  }

  logup(user: User): Observable<any> {
    user.password = 'adminadmin';
    return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.apiKey}`, user)
      .pipe(
        tap(this.setToken),
        catchError(this.handleError.bind(this))
      );
  }

  logout() {
    this.setToken(null)
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  private handleError(error: HttpErrorResponse){
    const {message} = error.error.error

    switch(message) {
      case 'INVALID_EMAIL':
        this.error$.next(message)
        break
      case 'INVALID_PASSWORD':
        this.error$.next(message)
        break
      case 'EMAIL_NOT_FOUND':
        this.error$.next(message)
        break
    }


    return throwError(error);
  }

  private setToken(response: FbAuthResponse | null) {
    if (response) {
      const expDate = new Date(new Date().getTime() + +response.expiresIn * 1000)
      localStorage.setItem('token', response.idToken)
      localStorage.setItem('token-exp', expDate.toString())
    } else {
      localStorage.clear();
    }
  }
}
