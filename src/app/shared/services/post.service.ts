/* tslint:disable:no-shadowed-variable */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CreateResponse, Post} from '@shared/interfaces';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class PostService {
  constructor(private http: HttpClient) {}

  create(post: Post): Observable<Post> {
    return this.http.post( environment.dbUrl + '/posts.json', post)
      .pipe(map((response: CreateResponse) => {
        return {
          ...post,
          id: response.name,
          date: new Date(post.date)};
      }));
  }

  update(post: Post): Observable<Post> {
    return this.http.patch<Post>( environment.dbUrl + '/posts/' + post.id + '.json', post);
  }

  getAll(): Observable<Post[]> {
    return this.http.get( environment.dbUrl + '/posts.json')
      .pipe(map(( response: {[key: string]: any} ) => {
        if (response) {
          return Object
            .keys(response)
            .map((key, index) => ({
              ...response[key],
              id: key,
              position: index + 1,
              date: new Date(response[key].date)
            }));
        }
      }));
  }

  getById(id: string): Observable<Post> {
    return this.http.get<Post>(environment.dbUrl + '/posts/' + id + '.json')
      .pipe(map((post: Post) => {
        if (post) {
          return {
            ...post, id,
            date: new Date(post.date)
          };
        }
    }));
  }

  remove(id: string): Observable<void> {
    return this.http.delete<void>(environment.dbUrl + '/posts/' + id + '.json');
  }
}
