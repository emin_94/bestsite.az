import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { WrapperComponent } from './wrapper/wrapper.component';

@NgModule({
  imports: [CommonModule, RouterModule, TranslateModule],
  declarations: [ WrapperComponent],
  exports: [ WrapperComponent],
})
export class LayoutsModule {}
