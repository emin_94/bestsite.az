import { NgModule } from '@angular/core';
import { MetaLoader, MetaModule, MetaStaticLoader, PageTitlePositioning } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

export function metaFactory(translate: TranslateService): MetaLoader {
  return new MetaStaticLoader({
    callback: (key: string): Observable<string | Object> => translate.get(key),
    pageTitlePositioning: PageTitlePositioning.AppendPageTitle,
    pageTitleSeparator: ' | ',
    applicationName: 'BESTSITE',
    defaults: {
      title: 'BESTSITE',
      description: 'We create best site for you',
      'og:site_name': 'BESTSITE',
      'og:type': 'website',
      'og:locale': 'ru_RU',
      'og:locale:alternate': [
        { code: 'ru', name: 'Русский', culture: 'ru-RU' },
        { code: 'az', name: 'Azerbaycan Dili', culture: 'az-AZ' },
      ]
        .map((lang: any) => lang.culture)
        .toString(),
    },
  });
}

@NgModule({
  imports: [
    MetaModule.forRoot({
      provide: MetaLoader,
      useFactory: metaFactory,
      deps: [TranslateService],
    }),
  ],
})
export class SharedMetaModule {}
