import {Component, OnInit} from '@angular/core';
import {query, style, transition, trigger} from '@angular/animations';
import {Observable} from 'rxjs';
import {ILang, TranslatesService} from '@shared/translates';
import {MatSidenavContent} from "@angular/material/sidenav";
import {element} from "protractor";
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
  animations: [
    trigger('routeAnimation', [
      transition('* <=> *', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%'
          })
        ]),
        query(':enter', [
          style({ left: '-100%'})
        ]),
      ])
    ])
  ]
})
export class MainLayoutComponent implements OnInit {
  public langList$: Observable<ILang[]>;
  public currentLang;
  public pageLoaded;

  isHome = false;
  opened = false;


  constructor(private _translatesService: TranslatesService, private router: Router) {}

  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      // Для показа новой страницы с верха
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });

    window.addEventListener('scroll', this.fartherOfTop, true);
    window.addEventListener('scroll', this.toolbarFarOfTopAndHome, true);
    this.langList$ = this._translatesService.getLangList();
    this.currentLang = this._translatesService.getCurrentLang();

    if (this.router.url === '/') {
      this.isHome = true;
    }
  }

  public changeLang(code: string): void {
    this._translatesService.changeLang(code);
    this.currentLang = code;
  }
  public fartherOfTop() {
    return window.pageYOffset > 800;
  }
  public toolbarFarOfTopAndHome() {
      return window.pageYOffset > 30;
  }





  public currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset) return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
      return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
  }


  public elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
      // @ts-ignore
      node = node.offsetParent;
      y += node.offsetTop;
    } return y;
  }


  public smoothScroll(eID) {
    var startY = this.currentYPosition();
    var stopY = this.elmYPosition(eID);
    var distance = stopY > startY ? stopY - startY : startY - stopY;
    if (distance < 100) {
      scrollTo(0, stopY); return;
    }
    var speed = Math.round(distance / 100);
    if (speed >= 20) speed = 20;
    var step = Math.round(distance / 25);
    var leapY = stopY > startY ? startY + step : startY - step;
    var timer = 0;
    if (stopY > startY) {
      for ( var i=startY; i<stopY; i+=step ) {
        setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
        leapY += step; if (leapY > stopY) leapY = stopY; timer++;
      } return;
    }
    for ( var i=startY; i>stopY; i-=step ) {
      setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
      leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
    }
  }





}
