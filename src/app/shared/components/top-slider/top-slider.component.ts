import { Component, OnInit } from '@angular/core';
import {SwiperConfigInterface} from "ngx-swiper-wrapper";
import {TranslatesService} from "@shared/translates";
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-top-slider',
  templateUrl: './top-slider.component.html',
  styleUrls: ['./top-slider.component.css']
})
export class TopSliderComponent implements OnInit {
  public currentLang;
  public config: SwiperConfigInterface = {
    loop: true,
    speed: 1300,
    autoplay: {
      delay: 7000,
      disableOnInteraction: false
    },
    effect: "fade",
    preloadImages: false,
    lazy: {
      loadPrevNext: false
    },
    breakpoints: {
      // when window width is <= 640px
      640: {
        slidesPerView: 1,
        spaceBetween: 0
      }
    }

  };

  slides = [
    {
      title: {
        ru: "BESTSITE",
        az: "BESTSITE"
      },
      description: {
        ru: "У нас вы сможете заказать - создание, продвижение и обновление сайтов.",
        az: "Xəyallarınızın peşəkar reallaşdırılması"
      },
      background: "/assets/images/backgrounds/top_bg_1.jpg"
    },
    {
      title: {
        ru: "Простота, удобство, эффективность",
        az: "BESTSITE"
      },
      description: {
        ru: "Созданные нами сайты получат простую, удобную и эффективную админ панель с помощью которой вы сможете без лишних усилий заполнить ваше приложение, а если у вас возникнут какие то вопросы просто обротитесь в нашу службу поддержки. ",
        az: "Xəyallarınızın peşəkar reallaşdırılması"
      },
      background: "/assets/images/backgrounds/top_bg_2.jpg",
      filter: true,
      style: 'text-align: left; margin-right: auto;'
    },
    {
      title: {
        ru: "Быстрые, красивые, современые",
        az: "BESTSITE"
      },
      description: {
        ru: "Мы создаем сайты используя самые лучшие технологии. Быстрые, красивые и современые сайты поднимут имидж ашего бизнеса на новый уровень !",
        az: "Biz veb aləmin ən son texnologiyalarını istifadə edərək sizin biznesiniz üçün mükəmməl, kreativ saytlar yaradırıq"
      },
      background: "/assets/images/backgrounds/top_bg_3.jpg"
    },
    {
      title: {
        ru: "Интернет магазин вашей мечты",
        az: "BESTSITE"
      },
      description: {
        ru: "Создадим интернет магазин с идеально настроенным seo с технической стороны, Функциональной понелью редактирования товаров, контролем маркетинга и другими запросами клиентов",
        az: "Xəyallarınızın peşəkar reallaşdırılması"
      },
      background: "/assets/images/backgrounds/top_bg_4.jpg",
      filter: true,
      style: 'margin-right: auto; text-align:left;',
      style_h2: 'margin-right: auto;'
    },
    {
      title: {
        ru: "Когда есть куда идти",
        az: "BESTSITE"
      },
      description: {
        ru: "Дорабатываем, улучшаем, разрабатываем модули и новые функции для существующих сайтов.",
        az: "Xəyallarınızın peşəkar reallaşdırılması"
      },
      background: "/assets/images/backgrounds/top_bg_5.jpg"
    }
  ]

  constructor(private _translatesService: TranslatesService, translate: TranslateService) {
    translate.onLangChange.pipe().subscribe((event: LangChangeEvent) => {
      this.currentLang = this._translatesService.getCurrentLang()
    });
  }

  ngOnInit(): void {
    this.currentLang = this._translatesService.getCurrentLang()
  }

}
