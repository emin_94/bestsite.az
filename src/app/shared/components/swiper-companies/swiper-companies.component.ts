import {Component, Input, OnInit} from '@angular/core';
import {SwiperConfigInterface} from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-swiper-companies',
  templateUrl: './swiper-companies.component.html',
  styleUrls: ['./swiper-companies.component.css']
})
export class SwiperCompaniesComponent implements OnInit {

  public companies = [
    {
      icon: '/assets/icons/companies/CloudLinux.svg',
      name: 'CloudLinux'
    },
    {
      icon: '/assets/icons/companies/cPanel.svg',
      name: 'cPanel'
    },
    {
      icon: '/assets/icons/companies/google-cloud.svg',
      name: 'google-cloud'
    },
    {
      icon: '/assets/icons/companies/bestsite.svg',
      name: 'google-cloud'
    },
    {
      icon: '/assets/icons/companies/KernelCare.svg',
      name: 'KernelCare'
    },
    {
      icon: '/assets/icons/companies/pashabank.png',
      name: 'pashabank'
    },
    {
      icon: '/assets/icons/companies/portmanat.svg',
      name: 'portmanat'
    },
    {
      icon: '/assets/icons/companies/visa.svg',
      name: 'visa'
    },
    {
      icon: '/assets/icons/companies/firebase.png',
      name: 'firebase'
    },
  ]

  public config: SwiperConfigInterface = {
    spaceBetween: 30,
    speed: 3000,
    loop: true,
    allowTouchMove: false,
    noSwiping: true,
    autoplay: {
      delay: 1,
    },
    hashNavigation: false,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      50: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      480: {
        slidesPerView: 2,
        spaceBetween: 30
      },
      640: {
        slidesPerView: 4,
        spaceBetween: 40
      },
      1000: {
        slidesPerView: 6,
        spaceBetween: 40
      }
    }
  };

  constructor() { }

  ngOnInit() {

  }

}
