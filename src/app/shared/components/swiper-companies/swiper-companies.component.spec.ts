import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwiperCompaniesComponent } from './swiper-companies.component';

describe('SwiperCompaniesComponent', () => {
  let component: SwiperCompaniesComponent;
  let fixture: ComponentFixture<SwiperCompaniesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwiperCompaniesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwiperCompaniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
