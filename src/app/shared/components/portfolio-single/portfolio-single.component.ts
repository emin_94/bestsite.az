import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-portfolio-single',
  templateUrl: './portfolio-single.component.html',
  styleUrls: ['./portfolio-single.component.scss']
})
export class PortfolioSingleComponent implements OnInit {
  // tslint:disable-next-line:variable-name
  @Input() image_src = '';
  @Input() title = '';
  @Input() url = '';
  constructor() { }

  ngOnInit(): void {
  }

}
