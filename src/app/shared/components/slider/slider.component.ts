import { Component, OnInit } from '@angular/core';
import { SwiperConfigInterface, SwiperPaginationInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  slide: {
    url: '/assets/images/8WVj08Oy0i5ayj6CxmYMwA.jpg'
  };
  public config: SwiperConfigInterface = {
    slidesPerView: 1,
    speed: 1000,
    autoplay: true,
    loop: true,
    spaceBetween: 30,
    grabCursor: true,
  };
  private pagination: SwiperPaginationInterface = {
    el: '.swiper-pagination',
    clickable: true,
    hideOnClick: false
  };
  constructor() { }

  ngOnInit(): void {
  }

}
