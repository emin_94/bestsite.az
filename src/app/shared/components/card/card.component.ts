import {Component, OnInit, Input, PLATFORM_ID, Inject} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  animations: [
    trigger('shadow', [
      state('begin', style({
        boxShadow: '-1px 7px 23px -6px rgba(0, 0, 0, 0.34)'
      })),
      state('end', style({
        boxShadow: '-1px 7px 23px -6px rgba(0, 0, 0, 0.14)',
      })),
      transition('begin => end', animate('0.3s')),
      transition('* => *', animate('0.3s'))
    ])
  ]
})
export class CardComponent implements OnInit {
  @Input() aosDelay: 0;
  @Input() image: '';
  @Input() title: '';
  @Input() text: '';
  @Input() aos_toggle: '';
  action = 'begin';

  isBrowser: boolean;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    if (isPlatformBrowser(this.platformId)) {
      this.isBrowser = true;
    }
  }

  ngOnInit(): void {
  }
}
