import { Component, OnInit, Optional, Inject, PLATFORM_ID } from '@angular/core';
import { RESPONSE, REQUEST } from '@nguniversal/express-engine/tokens';
import { isPlatformServer } from '@angular/common';
import { Request, Response } from 'express';
import {MetaService} from '@ngx-meta/core';
import {Meta} from '@angular/platform-browser';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

constructor(
    private readonly _meta: MetaService,
    private meta: Meta,
    @Optional() @Inject(REQUEST) private request: Request,
    @Optional() @Inject(RESPONSE) private response: Response,
    @Inject(PLATFORM_ID) private platformId: any) { }

  ngOnInit() {
    if (isPlatformServer(this.platformId)) {
      this.response.status(404);
    }

    this._meta.setTitle('page_not_found');
    this._meta.setTag('twitter:title', 'page_not_found');
    this._meta.setTag('site_name', 'BESTSITE');
    this._meta.setTag('og:type', 'website');
    this.meta.addTag({ name: 'image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'og:image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'twitter:image', content: '/assets/icons/bestsite_og_image.jpg'});
    this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image'});
    this.meta.addTag({ name: 'url', content: '//bestsite.az/not-found'});
    this.meta.addTag({ name: 'og:url', content: '//bestsite.az/not-found'});
    this.meta.addTag({ name: 'twitter:url', content: '//bestsite.az/not-found'});
  }
}
