export interface Environment {
  apiKey: string;
  production: boolean;
  isServer: boolean;
  host: string;
  dbUrl: string;
}
